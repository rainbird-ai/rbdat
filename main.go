package main

import (
	"compress/gzip"
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

func usage() {
	fmt.Printf("Usage: %s <filename>\n", os.Args[0])
}

func main() {
	var err error
	in := os.Stdin

	flag.Parse()
	switch flag.NArg() {
	case 0:
		// Nothing
	case 1:
		in, err = os.Open(flag.Arg(0))
		if err != nil {
			panic(err)
		}
	default:
		fmt.Println("Please provide 0-1 arguments")
		usage()
		os.Exit(1)
	}

	r, err := gzip.NewReader(in)
	if err != nil {
		panic(err)
	}

	var body map[string]interface{}
	err = json.NewDecoder(r).Decode(&body)
	if err != nil {
		panic(err)
	}

	enc := json.NewEncoder(os.Stdout)
	enc.SetIndent("", "  ")
	enc.Encode(&body)
}
